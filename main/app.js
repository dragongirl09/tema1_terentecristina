function distance(first, second) {
	//TODO: implementați funcția
	if (!Array.isArray(first) || !Array.isArray(second)) {
		throw new Error("InvalidType") //dintr-un motiv obscur nu se valideaza prin test, 
		//desi testat in consola browser-ului cu argumentele din test merge
	}
	else if (first.length === 0 && second.length === 0) {
		return 0;
	}
	else {
		let cont = 0
		const f = [...new Set(first)]
		const s = [...new Set(second)]
		f.forEach(e => {
			s.indexOf(e) === -1 ? cont++ : cont
		})
		s.forEach(e => {
			f.indexOf(e) === -1 ? cont++ : cont
		})
		return cont
	}
	//TODO: implement the function
}


module.exports.distance = distance